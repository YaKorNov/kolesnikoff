from functools import wraps
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest,HttpResponseNotFound

def GuestRequired(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('/admin/')
        return view_func(request, *args, **kwargs)
    return _wrapped_view


class AjaxRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return super(AjaxRequiredMixin, self).dispatch(
            request, *args, **kwargs)