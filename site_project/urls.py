# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib.auth.views import login,logout,password_reset,password_reset_done,password_reset_confirm,password_reset_complete
from utils import GuestRequired
from views import TagDetailView,BlogAjaxListView,HomePageDetailView,MainPagesDetailView, ConditionView, ContactsView, BlogDetailView, BlogListView

urlpatterns = [
    #url(r'', include('site_project.urls', 'site_project')),
    url(r'^$', HomePageDetailView.as_view(), {'main_page':True}, name = "home"),
    url(r'^article/(?P<slug>[-\w]+)$',MainPagesDetailView.as_view(), name="main_page_detail"),
    url(r'^cases_ajax/$',BlogAjaxListView.as_view(), name="cases_ajax"),
    url(r'^practice/$',BlogListView.as_view(), name="blog_list"),
    url(r'^practice/(?P<slug>[-\w]+)$',BlogDetailView.as_view(), name="blog_detail"),
    url(r'^tag/(?P<slug>[-\w]+)$',TagDetailView.as_view(), name="tag_detail"),
    url(r'^conditions/$',ConditionView.as_view()),
    url(r'^contacts/$',ContactsView.as_view()),
    url(r'^login/$',GuestRequired(login),name="login"),
    url(r'^logout/$',logout,
        {
            'template_name': 'registration/log_out.html'
        },
        name="logout"),
    url(r'^password/reset/$',GuestRequired(password_reset),{'post_reset_redirect': 'site_project:password_reset_done'},name="password_reset"),
    url(r'^password/reset/done/$',GuestRequired(password_reset_done),name="password_reset_done"),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',GuestRequired(password_reset_confirm),
        {
            'post_reset_redirect': 'site_project:password_reset_complete'
        },
        name="password_reset_confirm"),
    url(r'^reset/done/$',GuestRequired(password_reset_complete),name="password_reset_complete"),

]