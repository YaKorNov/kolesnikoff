# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from site_project.models import Hash_Tag,MainPages,Blog
from annoying.functions import get_object_or_None

class MainPagesAdminForm(forms.ModelForm):

    def clean_place_on_main_page(self):
        value = self.cleaned_data['place_on_main_page']
        if value:
            m_page = get_object_or_None(MainPages,place_on_main_page = True)
            if m_page and m_page.pk <> self.instance.pk :
                raise forms.ValidationError(u'Уже есть страница, помеченная для размещения на главной')
        return value


    class Meta:
        model = MainPages
        fields = ('title', 'slug', 'place_on_main_page', 'icon_image', 'material_excerpt', 'material', 'related_tag', 'comment')
        widgets = {
            'material': CKEditorWidget(),
            'material_excerpt': CKEditorWidget(),
        }

class BlogAdminForm(forms.ModelForm):

    class Meta:
        model = MainPages
        exclude = ('created','updated')
        widgets = {
            'material': CKEditorWidget(),
            'material_excerpt': CKEditorWidget(),
        }

class MainPagesAdmin(admin.ModelAdmin):
    form = MainPagesAdminForm
    prepopulated_fields = {"material": ("material_excerpt",)}
    list_display = ('title','place_on_main_page','comment')
    list_filter = ('title', )
    search_fields = ('title', 'slug')

class BlogAdmin(admin.ModelAdmin):
    form = BlogAdminForm
    list_display = ('title','comment')
    list_filter = ('title', )
    search_fields = ('title',)

class Hash_TagAdmin(admin.ModelAdmin):
    list_display = ('title', )
    list_filter = ('title', )
    search_fields = ('title', )


admin.site.register(Hash_Tag, Hash_TagAdmin)
admin.site.register(MainPages, MainPagesAdmin)
admin.site.register(Blog, BlogAdmin)

# Register your models here.
