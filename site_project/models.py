# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django_random_queryset import RandomManager

class Hash_Tag(models.Model):
    title = models.CharField(_(u'Название тега'), max_length=50, blank=False, unique = True)
    slug = models.CharField(_(u'Slug'), max_length=50, blank=False, error_messages={'blank':_(u"Поле Slug исользуется для привязки материала к URL")},
                            unique = True)

    class Meta:
        verbose_name = _(u'Хэш тэг')
        verbose_name_plural = _(u'Хэш тэги')

    def __unicode__(self):
        return u'%s' % (self.title)

class MainPages(models.Model):
    title = models.CharField(_(u'Название материала'), max_length=100, blank=False, )
    slug = models.CharField(_(u'Slug'), max_length=50, blank=False, error_messages={'blank':_(u"Поле Slug исользуется для привязки материала к URL")},
                            unique = True)
    place_on_main_page = models.BooleanField(_(u'Материал на главной странице'), default=False)
    material_excerpt = models.TextField(_(u'Вступление к материалу'), blank=False)
    material = models.TextField(_(u'Содержание материала'), blank=False, help_text = _(u"Текст материала, размещаемого на главной странице."))
    related_tag = models.ManyToManyField(Hash_Tag, verbose_name = _(u'Тэги'),)
    comment = models.CharField(_(u'Комментарий'), max_length=250, blank=True)
    icon_image = models.ImageField(_(u'Иконка материала'), upload_to='user_documents/')
    created = models.DateTimeField(_(u'Создана'), blank=True)
    updated = models.DateTimeField(_(u'Обновлена'), blank=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created = timezone.now()
        self.updated = timezone.now()
        super(MainPages, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _(u'Материал для главной страницы')
        verbose_name_plural = _(u'Материалы для главной страницы')

    def __unicode__(self):
        return u'%s' % (self.title)


class Blog(models.Model):
    title = models.CharField(_(u'Название материала'), max_length=100, blank=False, )
    slug = models.CharField(_(u'Slug'), max_length=50, blank=False, error_messages={'blank':_(u"Поле Slug исользуется для привязки материала к URL")},
                            unique = True)
    material_excerpt = models.TextField(_(u'Вступление к материалу'), blank=False)
    material = models.TextField(_(u'Содержание материала'), blank=False, )
    related_tag = models.ManyToManyField(Hash_Tag, verbose_name = _(u'Тэги'),)
    comment = models.CharField(_(u'Комментарий'), max_length=250, blank=True)
    created = models.DateTimeField(_(u'Создана'), blank=True)
    updated = models.DateTimeField(_(u'Обновлена'), blank=True)

    random_objects = RandomManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created = timezone.now()
        self.updated = timezone.now()
        super(Blog, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _(u'Запись для блога')
        verbose_name_plural = _(u'Записи для блога')

    def __unicode__(self):
        return u'%s' % (self.title)

