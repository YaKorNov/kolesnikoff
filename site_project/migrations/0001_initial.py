# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hash_Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0442\u0435\u0433\u0430')),
            ],
            options={
                'verbose_name': '\u0425\u044d\u0448 \u0442\u044d\u0433',
                'verbose_name_plural': '\u0425\u044d\u0448 \u0442\u044d\u0433\u0438',
            },
        ),
        migrations.CreateModel(
            name='MainPages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('slug', models.SlugField(unique=True, verbose_name='Slug', error_messages={b'blank': '\u041f\u043e\u043b\u0435 Slug \u0438\u0441\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u043f\u0440\u0438\u0432\u044f\u0437\u043a\u0438 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u043a \u0430\u0434\u0440\u0435\u0441\u0443'})),
                ('material', models.TextField(help_text='\u0422\u0435\u043a\u0441\u0442 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430, \u0440\u0430\u0437\u043c\u0435\u0449\u0430\u0435\u043c\u043e\u0433\u043e \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435.', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('comment', models.CharField(max_length=250, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
                ('created', models.DateTimeField(verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u0430', blank=True)),
                ('updated', models.DateTimeField(verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0430', blank=True)),
                ('related_tag', models.ManyToManyField(to='site_project.Hash_Tag')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b \u0434\u043b\u044f \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
                'verbose_name_plural': '\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b \u0434\u043b\u044f \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
        ),
    ]
