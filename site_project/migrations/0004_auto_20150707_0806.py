# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0003_auto_20150707_0752'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainpages',
            name='related_tag',
            field=models.ManyToManyField(to='site_project.Hash_Tag', verbose_name='\u0422\u044d\u0433\u0438'),
        ),
    ]
