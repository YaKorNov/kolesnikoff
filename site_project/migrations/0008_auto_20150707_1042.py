# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0007_mainpages_icon_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainpages',
            name='icon_image',
            field=models.ImageField(upload_to=b'user_documents/', verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430'),
        ),
    ]
