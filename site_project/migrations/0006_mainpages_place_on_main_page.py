# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0005_auto_20150707_0813'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpages',
            name='place_on_main_page',
            field=models.BooleanField(default=False, verbose_name='\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435'),
        ),
    ]
