# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0010_auto_20150707_1410'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('slug', models.CharField(unique=True, max_length=50, verbose_name='Slug', error_messages={b'blank': '\u041f\u043e\u043b\u0435 Slug \u0438\u0441\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u043f\u0440\u0438\u0432\u044f\u0437\u043a\u0438 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u043a URL'})),
                ('material_excerpt', models.TextField(verbose_name='\u0412\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435 \u043a \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0443')),
                ('material', models.TextField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('comment', models.CharField(max_length=250, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('created', models.DateTimeField(verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u0430', blank=True)),
                ('updated', models.DateTimeField(verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0430', blank=True)),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043f\u0438\u0441\u044c \u0434\u043b\u044f \u0431\u043b\u043e\u0433\u0430',
                'verbose_name_plural': '\u0417\u0430\u043f\u0438\u0441\u0438 \u0434\u043b\u044f \u0431\u043b\u043e\u0433\u0430',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438')),
                ('slug', models.CharField(unique=True, max_length=50, verbose_name='Slug', error_messages={b'blank': '\u041f\u043e\u043b\u0435 Slug \u0438\u0441\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u043f\u0440\u0438\u0432\u044f\u0437\u043a\u0438 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u043a URL'})),
            ],
        ),
        migrations.AlterField(
            model_name='mainpages',
            name='slug',
            field=models.CharField(unique=True, max_length=50, verbose_name='Slug', error_messages={b'blank': '\u041f\u043e\u043b\u0435 Slug \u0438\u0441\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u043f\u0440\u0438\u0432\u044f\u0437\u043a\u0438 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u043a URL'}),
        ),
        migrations.AddField(
            model_name='blog',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='site_project.Category'),
        ),
        migrations.AddField(
            model_name='blog',
            name='related_tag',
            field=models.ManyToManyField(to='site_project.Hash_Tag', verbose_name='\u0422\u044d\u0433\u0438'),
        ),
    ]
