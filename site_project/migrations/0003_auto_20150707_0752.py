# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0002_auto_20150707_0545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainpages',
            name='related_tag',
            field=models.ManyToManyField(db_constraint='\u0422\u044d\u0433\u0438', to='site_project.Hash_Tag'),
        ),
    ]
