# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0016_auto_20150713_1238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hash_tag',
            name='slug',
            field=models.CharField(unique=True, max_length=50, verbose_name='Slug', error_messages={b'blank': '\u041f\u043e\u043b\u0435 Slug \u0438\u0441\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u043f\u0440\u0438\u0432\u044f\u0437\u043a\u0438 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u043a URL'}),
        ),
    ]
