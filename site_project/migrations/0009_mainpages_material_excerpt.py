# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_project', '0008_auto_20150707_1042'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpages',
            name='material_excerpt',
            field=models.TextField(default=b'sd', verbose_name='\u0412\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435 \u043a \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0443'),
        ),
    ]
