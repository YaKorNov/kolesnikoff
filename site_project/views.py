# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import SingleObjectMixin
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.conf import settings
from utils import AjaxRequiredMixin

from .models import MainPages,Blog,Hash_Tag

class HomePageDetailView(DetailView):

    model = MainPages
    # slug_field = 'slug'
    template_name = 'site/home_page_detail.html'

    def get_object(self):
        main_page = self.kwargs.get('main_page', None)
        if main_page:
            return get_object_or_404(MainPages, place_on_main_page = True)
        return super(HomePageDetailView, self).get_object()

    def get_context_data(self, **kwargs):
        context = super(HomePageDetailView, self).get_context_data(**kwargs)
        context['articles_list'] = MainPages.objects.exclude(place_on_main_page=True).order_by('updated')[0:settings.MAX_COUNT_PAGES]
        #context['articles_list'] = MainPages.objects.exclude(place_on_main_page=True).order_by('updated')[0:4]
        return context


class MainPagesDetailView(DetailView):

    model = MainPages
    # slug_field = 'slug'
    template_name = 'site/main_page_detail.html'



class BlogDetailView(DetailView):

    model = Blog
    # slug_field = 'slug'
    template_name = 'site/blog_page_detail.html'



class BlogListView(ListView):

    model = Blog
    paginate_by = 1
    # slug_field = 'slug'
    template_name = 'site/blog_page.html'

    def get_context_data(self, **kwargs):
        cd = super(BlogListView, self).get_context_data(**kwargs)
        if self.request.is_ajax():
            cd.update({'base_template_name':"ajax_base.html" })
        else:
            cd.update({'base_template_name':"base.html" })
        return cd



class BlogAjaxListView(AjaxRequiredMixin,ListView):

    #model = Blog
    # slug_field = 'slug'
    template_name = 'site/blog_ajax_list.html'

    def get_queryset(self):
        #return Blog.objects.all()[0:self.request.GET.get('blog_pages_count',1)]
        return Blog.random_objects.random(10)[0:self.request.GET.get('blog_pages_count',1)]


class TagDetailView(SingleObjectMixin, ListView):

    paginate_by = 2
    template_name = "site/tag_detail.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Hash_Tag.objects.all())
        return super(TagDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TagDetailView, self).get_context_data(**kwargs)
        context['hash_tag'] = self.object
        if self.request.is_ajax():
            context['base_template_name'] = "ajax_base.html"
        else:
            context['base_template_name'] = "base.html"
        return context

    def get_queryset(self):
        return self.object.blog_set.all()



class ConditionView(TemplateView):
    template_name = "site/conditions.html"




class ContactsView(TemplateView):
    template_name = "site/contacts.html"