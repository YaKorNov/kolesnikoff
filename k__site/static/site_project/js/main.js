jQuery(document).ready(function() {

    jQuery('.alt.actions').click(function() {
        jQuery('.fadeLayer, .rLayer.actions').show();
        return false;
    });

    jQuery('body').on('click','.alt.cases',function(event) {
        event.preventDefault();
        jQuery('.fadeLayer, .rLayer.cases').show();
        jQuery.ajax({
            url: jQuery('.rLayer.cases').attr("cases_ajax_data_load_url"),
            type : 'GET',
            data: {'blog_pages_count': 5},
            success: function (data) {
                jQuery('.rLayer.cases #other_cases').html(data);
            }
        });
        return false;
    });

    jQuery('.rLayer span').click(function() {
        jQuery('.fadeLayer, .rLayer').hide();
        return false;
    });

    jQuery('body').on('click','.linked-pages',function(event) {
        event.preventDefault();
        jQuery.ajax({
            url: jQuery(this).attr('href'),
            type : 'GET',
            success: function (data) {
                jQuery('div.wrapper#content-block').html(data);
            }
        });
        return false;
    });


    jQuery(".video").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width       : '70%',
        height	: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none'
    });


});



